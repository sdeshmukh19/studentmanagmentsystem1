// student.js

class Student {
    constructor(name, age, id, address, phoneNumber, email) {
      this.name = name;
      this.age = age;
      this.id = id;
      this.address = address;
      this.phoneNumber = phoneNumber;
      this.email = email;
    }
  }
  
  module.exports = Student;
  