const fs = require('fs');
const readline = require('readline');

class Student {
    constructor(name, age, id, address, phoneNumber, email) {
        this.name = name;
        this.age = age;
        this.id = id;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
}

const studentList = [];
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function printStudents() {
    for (const student of studentList) {
        console.log("Name: " + student.name);
        console.log("Age: " + student.age);
        console.log("ID: " + student.id);
        console.log("Address: " + student.address);
        console.log("Phone Number: " + student.phoneNumber);
        console.log("Email: " + student.email);
        console.log();
    }
}

function addStudent() {
    const newStudent = new Student();
    rl.question("Enter student name: ", (name) => {
        newStudent.name = name.trim();
        if (newStudent.name === "") {
            console.log("Name cannot be empty.");
            return;
        }

        rl.question("Enter student age: ", (ageInput) => {
            const age = parseInt(ageInput);
            if (isNaN(age)) {
                console.log("Invalid age format. Age should be a number.");
                return;
            }
            newStudent.age = age;

            rl.question("Enter student ID: ", (idInput) => {
                const id = parseInt(idInput);
                if (isNaN(id)) {
                    console.log("Invalid ID format. ID should be a number.");
                    return;
                }
                newStudent.id = id;

                rl.question("Enter student address: ", (address) => {
                    newStudent.address = address.trim();
                    rl.question("Enter student phone number: ", (phoneNumberInput) => {
                        if (!phoneNumberInput.match(/^\d+$/)) {
                            console.log("Invalid phone number format. Phone number should only contain numbers.");
                            return;
                        }
                        newStudent.phoneNumber = phoneNumberInput;

                        rl.question("Enter student email: ", (email) => {
                            newStudent.email = email.trim();

                            // Check for duplicates based on ID
                            if (isIdUnique(newStudent.id)) {
                                studentList.push(newStudent);
                                console.log("Student added: " + newStudent.name);
                            } else {
                                console.log("Student with ID " + newStudent.id + " already exists.");
                            }
                        });
                    });
                });
            });
        });
    });
}

function isIdUnique(idToCheck) {
    return !studentList.some((student) => student.id === idToCheck);
}

rl.setPrompt('Student Management System Menu:\n1. Print Students\n2. Add Student\n3. Delete Student\n4. Update Student\n5. Search Student by Name\n6. Search Student by ID\n7. Load Students from File\n8. Save Students to File\n9. Exit\nEnter your choice: ');

rl.prompt();
rl.on('line', (input) => {
    switch (input.trim()) {
        case '1':
            printStudents();
            break;
        case '2':
            addStudent();
            break;
        case '3':
            deleteStudent
            break;
        case '4':
            updateStudent() 
            break;
        case '5':
            searchStudentByName()
            break;
        case '6':
            searchStudentById()
            break;
        case '7':
            loadStudentsFromFile()
            break;
        case '8':
            saveStudentsToFile() 
            break;
        case '9':
            rl.close();
            break;
        default:
            console.log('Invalid choice. Please try again.');
            break;
    }
    rl.prompt();
}).on('close', () => {
    console.log('Exiting the program.');
    process.exit(0);
});

function deleteStudent() {
    rl.question("Enter student name to delete: ", (nameToDelete) => {
        const indexToDelete = studentList.findIndex((student) => student.name === nameToDelete);
        if (indexToDelete !== -1) {
            studentList.splice(indexToDelete, 1);
            console.log(nameToDelete + " has been deleted.");
        } else {
            console.log(nameToDelete + " not found.");
        }
        rl.prompt();
    });
}

function updateStudent() {
    rl.question("Enter student name to update: ", (nameToUpdate) => {
        const studentToUpdate = studentList.find((student) => student.name === nameToUpdate);
        if (studentToUpdate) {
            rl.question("Enter new name: ", (newName) => {
                studentToUpdate.name = newName.trim();

                rl.question("Enter new age: ", (newAgeInput) => {
                    const newAge = parseInt(newAgeInput);
                    if (!isNaN(newAge)) {
                        studentToUpdate.age = newAge;

                        rl.question("Enter new ID: ", (newIdInput) => {
                            const newId = parseInt(newIdInput);
                            if (!isNaN(newId)) {
                                studentToUpdate.id = newId;

                                rl.question("Enter new address: ", (newAddress) => {
                                    studentToUpdate.address = newAddress.trim();

                                    rl.question("Enter new phone number: ", (newPhoneNumberInput) => {
                                        if (newPhoneNumberInput.match(/^\d+$/)) {
                                            studentToUpdate.phoneNumber = newPhoneNumberInput;

                                            rl.question("Enter new email: ", (newEmail) => {
                                                studentToUpdate.email = newEmail.trim();
                                                console.log(nameToUpdate + " has been updated.");
                                                rl.prompt();
                                            });
                                        } else {
                                            console.log("Invalid phone number format. Phone number should only contain numbers.");
                                            rl.prompt();
                                        }
                                    });
                                });
                            } else {
                                console.log("Invalid ID format. ID should be a number.");
                                rl.prompt();
                            }
                        });
                    } else {
                        console.log("Invalid age format. Age should be a number.");
                        rl.prompt();
                    }
                });
            });
        } else {
            console.log("Student not found: " + nameToUpdate);
            rl.prompt();
        }
    });
}

function searchStudentByName() {
    rl.question("Enter student name to search: ", (nameToSearch) => {
        const foundStudent = studentList.find((student) => student.name === nameToSearch);
        if (foundStudent) {
            console.log("Student found:");
            console.log("Name: " + foundStudent.name);
            console.log("Age: " + foundStudent.age);
            console.log("ID: " + foundStudent.id);
            console.log("Address: " + foundStudent.address);
            console.log("Phone Number: " + foundStudent.phoneNumber);
            console.log("Email: " + foundStudent.email);
        } else {
            console.log("Student not found: " + nameToSearch);
        }
        rl.prompt();
    });
}

function searchStudentById() {
    rl.question("Enter student ID to search: ", (idToSearchInput) => {
        const idToSearch = parseInt(idToSearchInput);
        if (!isNaN(idToSearch)) {
            const foundStudent = studentList.find((student) => student.id === idToSearch);
            if (foundStudent) {
                console.log("Student found:");
                console.log("Name: " + foundStudent.name);
                console.log("Age: " + foundStudent.age);
                console.log("ID: " + foundStudent.id);
                console.log("Address: " + foundStudent.address);
                console.log("Phone Number: " + foundStudent.phoneNumber);
                console.log("Email: " + foundStudent.email);
            } else {
                console.log("Student with ID " + idToSearch + " not found.");
            }
        } else {
            console.log("Invalid ID format. ID should be a number.");
        }
        rl.prompt();
    });
}

function loadStudentsFromFile() {
    rl.question("Enter the name of the file to load students from: ", (fileName) => {
        fs.readFile(fileName, 'utf8', (err, data) => {
            if (err) {
                console.error("Error reading file: " + err.message);
                rl.prompt();
                return;
            }

            const lines = data.split('\n');
            lines.forEach((line) => {
                const parts = line.trim().split(' ');
                if (parts.length >= 6) {
                    const student = new Student(
                        parts[0],
                        parseInt(parts[1]),
                        parseInt(parts[2]),
                        parts[3],
                        parts[4],
                        parts[5]
                    );
                    studentList.push(student);
                }
            });

            console.log("Students loaded from file: " + fileName);
            rl.prompt();
        });
    });
}

function saveStudentsToFile() {
    rl.question("Enter the name of the file to save students to: ", (fileName) => {
        const fileContents = studentList.map((student) =>
            `${student.name} ${student.age} ${student.id} ${student.address} ${student.phoneNumber} ${student.email}`
        ).join('\n');

        fs.writeFile(fileName, fileContents, (err) => {
            if (err) {
                console.error("Error writing to file: " + err.message);
            } else {
                console.log("Students saved to file: " + fileName);
            }
            rl.prompt();
        });
    });
}
